import java.io.File;
import java.util.Collections;

import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;

public class MainOne {

	public static void main(String[] args) {
		InvocationRequest request = new DefaultInvocationRequest();
		
		File file = new File(new File("").getAbsolutePath() + File.separatorChar + "pom.xml");
		System.out.println("Running Maven pom file here:  " + file);
		request.setPomFile(file);
		request.setGoals(Collections.singletonList("install"));
		Invoker invoker = new DefaultInvoker();
		invoker.setMavenHome(new File(System.getenv("MVN_HOME")));
		try {
			invoker.execute(request);
		} catch (MavenInvocationException e) {
			e.printStackTrace();
		}
	}

}

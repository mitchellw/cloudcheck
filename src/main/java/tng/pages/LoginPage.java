package tng.pages;

/**
 * Created by mwright on 6/20/2016.
 */
import com.perfectomobile.driver.AppiumDriverExtended;
import com.perfectomobile.test.ConsoleReporter;
import com.perfectomobile.utils.PerfectoUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by mitchellw on 6/9/2016.
 */
public class LoginPage extends BasePage {
    @FindBy(xpath = "//*[@class='usernameInputWrap']/label")
    private WebElement usernameLabel;

    @FindBy(xpath = "//input[@name='cif']")
    private WebElement usernameField;

    @FindBy(xpath = "(//*[contains(text(),'Remember Me')]/following::TNG-LOADING-BUTTON/button)[1]")
    private WebElement goBtn;

    @FindBy(xpath = "//*[@class='unterHeader']/span")
    private WebElement secretQuestionTitle;

    @FindBy(xpath = "//*[@id='secretQuestionContainer']/descendant::span[1]")
    private WebElement secretQuestionField;

    @FindBy(xpath = "//INPUT[@name='secretAnswer']")
    private WebElement secretAnswerField;

    @FindBy(xpath = "(//BUTTON[descendant::*[contains(text(),'Next')]])[1]")
    private WebElement secretAnswerNext;

    @FindBy(xpath = "//input[@name='pin']")
    private WebElement pinInput;

    @FindBy(xpath = "//*[@id='pinContainer']/div/div[2]/tng-loading-button/button")
    private WebElement pinNext;

    @FindBy(xpath = "//*[@id=\"tosContainer\"]/div/div[2]/tng-switch/div/span")
    private WebElement termsToggleAccept;

    @FindBy(xpath = "//*[@id=\"tosContainer\"]//button[text()='Next']")
    private WebElement termsNext;

    /**********************************************************************
     * 		Constructor
     * ********************************************************************.
     *
     * @param driver the driver
     */
    public LoginPage(AppiumDriverExtended driver){
        super(driver);
        PerfectoUtils.closeApp("Tangerine", driver);
        PerfectoUtils.startApp("Tangerine", driver);
        reportTimeTillVisibile(usernameLabel, "Client / Card Number, or Username:");
    }

    public LoginPage GO(String username) {
        typeUsername(username).clickGoBtn();
        return this;
    }

    private void clickGoBtn() {
        clickElement(goBtn);
        reportTimeTillVisibile(secretQuestionTitle, "Secret Question");
    }

    public LoginPage typeUsername(String username) {
        clearAndType(usernameField, username);
        return this;
    }

    public LoginPage typeSecretAnswer() {

        String questionTxt = getElementWhenClickable(secretQuestionField).getText();
        String answerStr = "automation";
        if (questionTxt.equals("What is the name of your childhood best friend?"))
            answerStr+="3";
        else if (questionTxt.equals("What was your childhood nickname?"))
            answerStr+="1";
        else if (questionTxt.equals("What was your favourite childhood story?")
                || questionTxt.equals("Quelle était votre histoire préférée lorsque vous étiez enfant ?"))
            answerStr+="2";
        else answerStr+="1";
        clearAndType(secretAnswerField, answerStr);
        clickElement(secretAnswerNext);

        reportTimeTillVisibile(secretQuestionTitle, "Picture and Phrase");

        return this;
    }

    public LoginPage typePIN(String number){
        clickElement(pinInput);
        getElementWhenClickable(pinInput).sendKeys(number);
        clickElement(pinNext);
        return this;
    }

    public HomePage acceptTOS() {
        try {
            return new HomePage(driver, 10);
        } catch (Exception e){
            try {
                clickElement(termsToggleAccept, 2);
                clickElement(termsNext);
            } catch (Exception e1) {
                ConsoleReporter.logThread("TOS page NOT displayed!");
            }
        }
        return new HomePage(driver, 20);
    }
}

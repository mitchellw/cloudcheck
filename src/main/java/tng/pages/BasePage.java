package tng.pages;

/**
 * Created by mwright on 6/20/2016.
 */
import java.util.concurrent.TimeUnit;

import com.perfectomobile.driver.AppiumDriverExtended;
import com.perfectomobile.test.ConsoleReporter;
import com.perfectomobile.utils.PopUpUtils;
import com.perfectomobile.utils.WindTunnelUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class BasePage {
    protected AppiumDriverExtended driver;

    @FindBy(xpath = "//*[@text='Log Off' or @name='Log Off']")
    private WebElement logOffBtn;

    @FindBy(xpath = "//*[@label='sidemenu icon']")
    private WebElement hamburgerBtn;

    private int timeout = 30;

    public BasePage(AppiumDriverExtended driver, int timeout) {
        this(driver);
        this.timeout = timeout;
    }

    public BasePage(AppiumDriverExtended driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
        driver.manage().timeouts().pageLoadTimeout(5, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
    }

    public static StopWatch startTimer() {
        StopWatch timer = new StopWatch();
        timer.start();
        return timer;
    }

    public WebElement getElementWhenClickable(WebElement element){
        return getElementWhenClickable(element, timeout);
    }

    public WebElement getElementWhenClickable(WebElement element, int timeout) {
        try {
            //ConsoleReporter.logThread("WAITING for element to be Clickable...");
            driver.setContext(AppiumDriverExtended.ContextType.WEBVIEW);
            return new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
        } catch (WebDriverException e) {
            System.err.println();
            ConsoleReporter.logError("ELEMENT NOT FOUND with " + timeout + " seconds: " + element + "\n\tERROR: " + e.getMessage(), 2);
            throw e;
        }
    }

    public void clickOkPopUp() {
        new PopUpUtils(driver)
                .addNativePopupBtns(By.xpath("//*[@text='OK' or @name='Ok']"))
                .waitForPopupAndClick(5);
    }

    public void clickElement(WebElement element) {
        clickElement(element, timeout);
    }

    public void clickElement(WebElement element, int timeout) {
        getElementWhenClickable(element).click();
    }

    public void clearAndType(WebElement element, String sendKeys) {
        WebElement foundElement = getElementWhenClickable(element);
        foundElement.clear();
        foundElement.sendKeys(sendKeys);
    }

    public static long getTimerSeconds(StopWatch timer) {

        return TimeUnit.SECONDS.convert(timer.getNanoTime(), TimeUnit.NANOSECONDS);
    }

    protected void reportTimeTillVisibile(WebElement element, String textToFind) {
        StopWatch statusTimer = startTimer();
        new WebDriverWait(driver, timeout).until(ExpectedConditions.textToBePresentInElement(element, textToFind));
        long timeElapsed = getTimerSeconds(statusTimer);
        System.out.println();
        ConsoleReporter.logError(timeElapsed + " seconds elapsed waiting for element: " + element + " to be visible.");
        WindTunnelUtils.reportTimer(driver, timeElapsed, 2000, textToFind, textToFind + " Timer");
    }

}

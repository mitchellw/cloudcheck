package tng.pages;

import com.perfectomobile.driver.AppiumDriverExtended;
import com.perfectomobile.utils.PerfectoUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by mwright on 6/20/2016.
 */
public class HomePage extends BasePage {


    @FindBy(xpath = "//*[@id='menuHeaderTitle']")
    private WebElement accountsHeader;

    @FindBy(xpath = "(//div[@id='menuButtonContainer'])[1]")
    private WebElement hamburgerBtn;

    @FindBy(xpath = "//*[text()='Log Me Out']")
    private WebElement logOutLink;


    public HomePage(AppiumDriverExtended driver, int timeout) {
        super(driver, timeout);
        reportTimeTillVisibile(accountsHeader, "Accounts");
    }

    public void logOff() {
        clickElement(hamburgerBtn);
        clickElement(logOutLink);
        PerfectoUtils.closeApp("Tangerine", driver);
    }
}

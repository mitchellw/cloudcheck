package com.perfectomobile.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.google.common.base.Function;
import com.perfectomobile.test.ResultsReporter;

/**
 * @author mitchellw@perfectomobile.com
 *
 */
public class PopUpUtils {

	public enum Context {
		NATIVE_APP, WEBVIEW, VISUAL
	}

	LinkedHashMap<By, Context> popupBtns;
	RemoteWebDriver driver;
	int implicitTimeout = 30;
	int pageTimeout = 30;
	private boolean logMsgs = true;

	/** The thread pool. */
	private static ExecutorService threadPool = Executors.newCachedThreadPool();
	
	/**
	 * Only want one asynchronous popUp check running in application 
	 * need to minimize timeout and context switching thread safety issues
	 */
	private static PopUpCheckThread popUpCheck;

	/**
	 * Show console log messages that indicate when class is looking for
	 * elements
	 * 
	 * @param show
	 *            (true to output debug messages)
	 */
	public void showLogMsgs(boolean show) {
		logMsgs = show;
	}

	/**
	 * Set existing implicit and pageTimeout waits waits will be reset to these
	 * after looking for popups Any existing asynchronous popup checks will be
	 * stopped
	 * 
	 * @param driver
	 * @param implicitTimeout
	 * @param pageTimeout
	 */
	public PopUpUtils(RemoteWebDriver driver, int implicitTimeout, int pageTimeout) {
		this(driver);
		this.implicitTimeout = implicitTimeout;
		this.pageTimeout = pageTimeout;
	}

	/**
	 * Default constructor Will reset implicit and pageTimeout waits to 30 sec
	 * after running unless other contructor is used Any existing asynchronous
	 * popup checks will be stopped
	 * 
	 * @param driver
	 */
	public PopUpUtils(RemoteWebDriver driver) {
		stopAsynchChecks();
		this.driver = driver;
		popupBtns = new LinkedHashMap<By, Context>();

	}

	/**
	 * Get all popup buttons current stored in map
	 *
	 * @return
	 */
	public LinkedHashMap<By, Context> getPopupBtns() {
		return popupBtns;
	}

	/**
	 * Set map of by locators and contexts to look for
	 * 
	 * @param popupBtns
	 */
	public void setPopupBtns(LinkedHashMap<By, Context> popupBtns) {
		this.popupBtns = popupBtns;
	}
	
	/**
	 * Set By locators with a Native context
	 * 
	 * @param locators
	 * @return
	 */
	public PopUpUtils setNativePopupBtns(By... locators) {
		popupBtns = new LinkedHashMap<By, Context>();
		for (By btn : locators) {
			popupBtns.put(btn, Context.NATIVE_APP);
		}
		return this;
	}

	/**
	 * Add By locators with a Native context
	 * 
	 * @param locators
	 * @return
	 */
	public PopUpUtils addNativePopupBtns(By... locators) {
		for (By btn : locators) {
			popupBtns.put(btn, Context.NATIVE_APP);
		}
		return this;
	}

	/**
	 * Set By locators with a Webview context
	 * 
	 * @param locators
	 * @return
	 */
	public PopUpUtils setWebViewPopupBtns(By... locators) {
		popupBtns = new LinkedHashMap<By, Context>();
		for (By btn : locators) {
			popupBtns.put(btn, Context.WEBVIEW);
		}
		return this;
	}
	
	/**
	 * Add By locators with a Webview context
	 * 
	 * @param locators
	 * @return
	 */
	public PopUpUtils addWebViewPopupBtns(By... locators) {
		for (By btn : locators) {
			popupBtns.put(btn, Context.WEBVIEW);
		}
		return this;
	}

	/**
	 * Add locator and context to element list
	 * 
	 * @param by
	 * @param context
	 * @return
	 */
	public PopUpUtils addToPopupBtns(By by, Context context) {
		popupBtns.put(by, context);
		return this;
	}

	/**
	 * Repeatedly looks for elements to click on another thread WARNING!!!
	 * Context and timeout switching are NOT threadsafe This could cause main
	 * thread to look for elements in wrong context or timeout
	 * 
	 * @param waitBtwnChecks
	 * @param expectedNumOfPopups
	 * @param totalChecks
	 */
	public void checkAsynchronously(int waitBtwnChecks, int expectedNumOfPopups, int totalChecks) {
		stopAsynchChecks();
		logThread("Starting Async Checks!");
		popUpCheck = new PopUpCheckThread(waitBtwnChecks, expectedNumOfPopups, totalChecks);
		threadPool.submit(popUpCheck);
	}

	/**
	 * Stop asynchronous checking running on separate thread
	 */
	public void stopAsynchChecks() {
		if (popUpCheck != null && popUpCheck.isRunning()) {
			logThread("Stopping Async Checks!");
			popUpCheck.stop();
			while (popUpCheck.isRunning()) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
				}
			}
		}
	}

	/**
	 * Set page and implicit timeout to zero so they dont interfere with other
	 * waits
	 */
	public void zeroTimeouts() {
		driver.manage().timeouts().pageLoadTimeout(0, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
	}

	protected void resetTimeouts() {
		driver.manage().timeouts().pageLoadTimeout(pageTimeout, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(implicitTimeout, TimeUnit.SECONDS);
	}

	/**
	 * Look through map of locators and clicks on first one found 
	 * No wait is used unlike waitForPopupAndClick function
	 */
	public boolean findAndClickPopup() {
		return waitForPopupAndClick(0);
	}

	/**
	 * Click on any element found in popup btn map and repeat expectedClicks
	 * times
	 * 
	 * @param expectedClicks
	 * 			(number of times to look for popups)
	 * @param waitTimeoutSec
	 *          (FluentWait timeout in seconds)
	 */
	public void clickMulitplePopUps(int expectedClicks, int waitTimeoutSec) {
		for (int i = 0; i < expectedClicks; i++) {
			waitForPopupAndClick(waitTimeoutSec);
		}
	}

	/**
	 * Look for elements in list and click if found
	 * 
	 * @param waitTimeoutSec
	 *            (FluentWait timeout in seconds)
	 * @return
	 */
	public synchronized boolean waitForPopupAndClick(int waitTimeoutSec) {
		Context previousCtx = getCurrentContextHandle();
		Wait<RemoteWebDriver> wait = new FluentWait<RemoteWebDriver>(driver).withTimeout(waitTimeoutSec, TimeUnit.SECONDS)
				.pollingEvery(1000, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class,
						 StaleElementReferenceException.class);
		try {
			WebElement popup = wait.until(new Function<RemoteWebDriver, WebElement>() {
				public WebElement apply(RemoteWebDriver driver) {
					return findPopupBtns();
				}
			});
			if (popup != null) {
				popup.click();
				logThread("Clicked found btn!");
				return true;
			}
		} catch (Exception e) {
			logThread(e.getMessage().split("\n")[0]);
			logThread("Did NOT find any popup btns!");
		} finally {
			switchToContext(previousCtx);
		}
		return false;
	}

	private WebElement findPopupBtns() {
		Context currentCtx = null;
		for (Entry<By, Context> locator : popupBtns.entrySet()) {
			logThread("Looking for btn: " + locator.getKey().toString());
			try {
				if (currentCtx != locator.getValue()) {
					switchToContext(locator.getValue());
					currentCtx = locator.getValue();
				}
				zeroTimeouts();
				List<WebElement> foundElement = driver.findElements(locator.getKey());
				if (foundElement.size() > 0) {
					logThread("Found btn: " + locator.getKey().toString());
					return foundElement.get(0);
				}
			} catch (Exception e) {
			} finally {
				resetTimeouts();
			}
		}
		return null;
	}

	/**
	 * Switch the {@link RemoteWebDriver} driver context. A list with possible
	 * contexts can be generated by the
	 * {@link #getCurrentContextHandle(RemoteWebDriver)} method. To check the
	 * current context, use the
	 * {@link #getCurrentContextHandle(RemoteWebDriver)} method.
	 * 
	 * @param driver
	 *            The context of the passed driver will be changed
	 * @param context
	 *            The context to change to
	 */
	public void switchToContext(Context context) {
		try {
			RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
			Map<String, String> params = new HashMap<String, String>();
			params.put("name", context.name());
			executeMethod.execute(DriverCommand.SWITCH_TO_CONTEXT, params);
		} catch (Exception e) {
			logThread("Could not switch context");
		}
	}

	/**
	 * Gets the {@link String} value of the current context of the driver. In
	 * order to change the current context, use the
	 * {@link #switchToContext(RemoteWebDriver, String)} method.
	 * 
	 * @param driver
	 *            The driver to get the context from.
	 * @return {@link String} value of the current context.
	 */
	public Context getCurrentContextHandle() {
		try {
			RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
			String context = (String) executeMethod.execute(DriverCommand.GET_CURRENT_CONTEXT_HANDLE, null);
			if (context.toLowerCase().contains("webview"))
				return Context.WEBVIEW;
			else if (context.toLowerCase().contains("native"))
				return Context.NATIVE_APP;
			else if (context.toLowerCase().contains("visual"))
				return Context.VISUAL;
		} catch (Exception e) {
			logThread("Could not get context!");
		}
		return Context.WEBVIEW;
	}
	
	public void clearNewChromePages() {
		setNativePopupBtns(
				By.xpath("//*[contains(@class,'Button') and (contains(@text,'Accept') or contains(@text, 'OK'))]"),
			    By.xpath("//*[contains(@class,'Button') and (contains(@text,'No') or contains(@text,'Next'))]"),
			    By.xpath("//*[contains(@class,'Button') and (contains(@text,'Allow') or contains(@text,'No') or contains(@text,'Done'))]"));
		//.addWebViewPopupBtns(locators);
		showLogMsgs(false);
		clickMulitplePopUps(3, 1);
	}

	private void logThread(String msg) {
		if (logMsgs)
			ResultsReporter.logThread("(PopUpUtils) " + msg);
	}

	/**
	 * The Class PopUpCheckThread.
	 */
	private class PopUpCheckThread implements Runnable {

		/** The time out. */
		private int secWaitBetweenChecks;
		private int expectedNumOfPopups;
		private int totalChecks;
		private boolean running;
		private boolean stop;

		/**
		 * Instantiates a new PopUpCheck thread.
		 *
		 * @param time
		 *            to wait between checks in seconds
		 * @param number
		 *            of popups expected to click (will stop if this many found)
		 * @param how
		 *            many popups to try and click before stopping (will stop
		 *            after this many checks)
		 */
		public PopUpCheckThread(int secWaitBetweenChecks, int expectedNumOfPopups, int totalChecks) {
			this.secWaitBetweenChecks = secWaitBetweenChecks * 1000;
			this.expectedNumOfPopups = expectedNumOfPopups;
			this.totalChecks = totalChecks;
		}

		public boolean isRunning() {
			return running;
		}

		public void stop() {
			stop = true;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Runnable#run()
		 */
		public void run() {
			int numChecks = 0;
			int popupsFound = 0;
			while (!stop && popupsFound < expectedNumOfPopups && numChecks < totalChecks && driver != null) {
				asyncLog("Asynchronous PopUp Check #" + ++numChecks);
				running = true;
				if (findAndClickPopup())
					popupsFound++;
				try {
					Thread.sleep(secWaitBetweenChecks);
				} catch (InterruptedException e) {
					asyncLog("Interupting sleep...");
					stop = true;
				}
			}
			asyncLog("Check Completed!");
			running = false;
		}

		void asyncLog(String msg) {
			if (logMsgs)
				System.out.println("Async PopUp Thread: " + msg);
		}
	}
}

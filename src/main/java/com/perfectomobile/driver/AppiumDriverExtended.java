package com.perfectomobile.driver;

import com.perfectomobile.httpclient.Credentials;
import com.perfectomobile.httpclient.HttpClientException;
import com.perfectomobile.httpclient.ParameterValue;
import com.perfectomobile.httpclient.device.DeviceParameter;
import com.perfectomobile.httpclient.device.DeviceResult;
import com.perfectomobile.httpclient.device.DevicesHttpClient;
import com.perfectomobile.test.ConsoleReporter;
import io.appium.java_client.AppiumDriver;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

/**
 * @author mitchellw
 *
 */
public class AppiumDriverExtended extends AppiumDriver<WebElement> {

	public enum ContextType { WEBVIEW, VISUAL, NATIVE_APP }

	private Capabilities allCapabilities;
	private ContextType context;

	/**
	 * @param remoteAddress
	 * @param desiredCapabilities
	 */
	public AppiumDriverExtended(URL remoteAddress, DesiredCapabilities desiredCapabilities) {
		super(remoteAddress, desiredCapabilities);
		org.apache.log4j.BasicConfigurator.configure(new org.apache.log4j.varia.NullAppender());
		updateCapabilities(desiredCapabilities);
		addDeviceProperties(desiredCapabilities);
		ConsoleReporter.setThreadName(allCapabilities);
		ConsoleReporter.logThread("DRIVER RETURNED WITH:\n\t" + allCapabilities);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.openqa.selenium.remote.RemoteWebDriver#getCapabilities()
	 */
	@Override
	public Capabilities getCapabilities() {
		return allCapabilities;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.openqa.selenium.remote.RemoteWebDriver#executeScript(java.lang.
	 * String, java.lang.Object[])
	 */
	@Override
	public Object executeScript(String script, Object... args) {
		String params = StringUtils.join(Arrays.asList(args), "\n");
		setContext(ContextType.VISUAL);
		System.out.println();
		ConsoleReporter
				.logThread("Running command: " + script + "\n\tWith parameters: " + params + ", CONTEXT: " + context);
		return super.executeScript(script, args);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.openqa.selenium.remote.RemoteWebDriver#get(java.lang.String)
	 */
	@Override
	public void get(String url) {
		//if ("android".equalsIgnoreCase(getCapabilities().getCapability("os") + ""))
			//new PopUpUtils(this).clearNewChromePages();
		setContext(ContextType.WEBVIEW);
		if (isDevice()) {
			ConsoleReporter.logThread("Resetting device browser by opening google first...");
			super.get("http://www.google.com");
		} else if (!url.startsWith("http"))
			url = "http://" + url;
		ConsoleReporter.logThread("Opening URL: " + url);
		super.get(url);
	}

	@Override
	protected WebElement findElement(String by, String using) {
		List<WebElement> allElements = findElements(by, using);
		if (allElements == null || allElements.isEmpty())
			throw new NoSuchElementException("Cannot locate an element using: " + using);
		return allElements.get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.openqa.selenium.remote.RemoteWebDriver#findElement(org.openqa.
	 * selenium.By)
	 */
	private String lastElementTxt = "";

	@Override
	public WebElement findElement(By by) {
		String elementTxt = "";
		try {
			if (getContextType() != ContextType.WEBVIEW || getContextType() != ContextType.NATIVE_APP)
				setContext(ContextType.WEBVIEW);
			WebElement element = super.findElement(by);
			elementTxt = "Found Element: " + by;
			if (!StringUtils.isEmpty(element.getText()))
				elementTxt += "\n\tElement contains text: " + element.getText();
			return element;
		} catch (Exception e) {
			elementTxt = "Trying to find element: " + by + " With CONTEXT: " + context;
			throw e;
		} finally {
			if (!elementTxt.equals(lastElementTxt)) {
				System.out.println();
				ConsoleReporter.logThread(elementTxt);
			} else
			lastElementTxt = elementTxt;
		}
	}

	@Override
	public String getContext() {
		if (context == null) {
			return super.getContext();
		}
		return context.toString();
	}

	public ContextType getContextType() {
		return context;
	}

	public void setContext(ContextType context) {
		if (context == null)
			return;
		this.context = context;
		if (isDevice())
			super.context(context.toString());
	}

	@Override
	public WebElement scrollTo(String text) {
		scrollToText(text);
		return this.findElementByXPath(String.format("(//*[contains(@text,'%s')])[1]", text));
	}

	@Override
	public WebElement scrollToExact(String text) {
		scrollToText(text);
		return this.findElementByXPath(String.format("(//*[@text='%s'])[1]", text));
	}

	/**
	 * Checks if is device.
	 *
	 * @param driver
	 *            capabilities
	 * @return true, if is device
	 */
	public boolean isDevice() {
		return isDevice(allCapabilities);
	}

	private boolean isDevice(Capabilities caps) {
		// first check if driver is a mobile device:
		return !isDesktopBrowser(caps) && caps.getCapability("deviceName") != null;
	}

	public boolean isDesktopBrowser() {
		return isDesktopBrowser(allCapabilities);
	}

	private boolean isDesktopBrowser(Capabilities caps) {
		// first check if deviceName set to browser name which triggers desktop:
		Object platformName = caps.getCapability("platformName") == null ? caps.getPlatform()
				: caps.getCapability("platformName") == null;
		try {
			return Platform.fromString(platformName + "") != Platform.ANY;
		} catch (Exception e) {
			return false;
		}
	}

	public WebElement fluentWait(By locator, long timeout) {
		ConsoleReporter.logThread(
				"Waiting " + timeout + "sec for PRESENSE of element: " + locator + ", with Context: " + context);
		return new WebDriverWait(this, timeout).until(ExpectedConditions.presenceOfElementLocated(locator));
	}

	public void scrollToText(String text) {
		Map<String, Object> params5 = new HashMap<>();
		params5.put("content", text);
		params5.put("scrolling", "scroll");
		params5.put("maxscroll", 5);
		params5.put("next", "SWIPE_UP");
		Object result = executeScript("mobile:text:find", params5);
		if (!"true".equals(result))
			throw new WebDriverException("Could NOT scroll to text: " + text + " NOT FOUND!!");
	}

	private void updateCapabilities(DesiredCapabilities desiredCapabilities) {

		try {
			for (Entry<String, ?> cap : super.getCapabilities().asMap().entrySet()) {
				if (desiredCapabilities.getCapability(cap.getKey()) == null
						|| desiredCapabilities.getCapability(cap.getKey()) == "") {
					desiredCapabilities.setCapability(cap.getKey(), cap.getValue());
				}
			}
		} catch (Exception e) {
			ConsoleReporter.logError(e.getMessage());
		}
		allCapabilities = desiredCapabilities;
	}

	private void addDeviceProperties(DesiredCapabilities desiredCapabilities) {

		if (!isDevice(desiredCapabilities))
			return;

		Credentials credentials = new Credentials(desiredCapabilities.getCapability("user").toString(),
				desiredCapabilities.getCapability("password").toString());

		String host = getRemoteAddress().toString().replace("https://", "")
				.replace("/nexperience/perfectomobile/wd/hub", "");

		DevicesHttpClient client = new DevicesHttpClient(host, credentials);
		List<ParameterValue> inputParameters = new LinkedList<>();
		inputParameters.add(new ParameterValue("deviceId", desiredCapabilities.getCapability("deviceName").toString()));
		DeviceResult device = null;
		try {
			device = client.listDevices(inputParameters, false).get(0);
		} catch (HttpClientException e1) {
			ConsoleReporter.logError(e1.getMessage());
		}
		for (DeviceParameter test : DeviceParameter.values()) {
			desiredCapabilities.setCapability(test.toString(), device.getResponseValue(test));
		}
		allCapabilities = desiredCapabilities;
	}

	private String getTimerString(StopWatch timer) {
		return "(" + TimeUnit.SECONDS.convert(timer.getNanoTime(), TimeUnit.NANOSECONDS) + "s) ";
	}
}

package com.perfectomobile.test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.Reporter;
import com.perfectomobile.dataDrivers.excelDriver.ExcelDriver;
import com.perfectomobile.utils.PerfectoUtils;

public class ResultsReporter {
	
	/**
	 * The {@link RemoteWebDriver} driver is used for communication with the device under test
	 */
	protected RemoteWebDriver driver;
	
	/**
	 * The {@link ExcelDriver} sheet is used for writing the full detail test results to Excel DB
	 */
	protected static ExcelDriver detailedResultSheet;
	
	/**
	 * The {@link DesiredCapabilities} caps stores the capabilities for every device under test
	 */
	protected Capabilities caps;
	
	/**
	 * The device description as it's stored on the Perfecto Mobile cloud
	 */
	public String getDeviceDesc() {
		return caps.getCapability("model") + " " + caps.getCapability("deviceName");
	}
	
	/**
	 * The name of the test function
	 */
	protected String testName;
	
	public RemoteWebDriver getDriver() {
		return driver;
	}

	public void setDriver(RemoteWebDriver driver) {
		this.driver = driver;
	}

	public Capabilities getCaps() {
		return caps;
	}

	public void setCaps(Capabilities caps) {
		this.caps = caps;
	}

	public static ExcelDriver getDetailedResultSheet() {
		return detailedResultSheet;
	}

	public void setDetailedResultSheet(ExcelDriver detailedResultSheet) {
		this.detailedResultSheet = detailedResultSheet;
	}
	
	public ResultsReporter(Capabilities capabilities) {
		this(capabilities, null, capabilities.getCapability("scriptName") + "");
		testName += "[" + getDeviceDesc() + "]";
	}
	
	public ResultsReporter(Capabilities capabilities, String testName) {
		this(capabilities, null, testName);
	}
	
	public ResultsReporter(RemoteWebDriver driver, String testName) {
		this(driver.getCapabilities(), driver, testName);
	}
	
	public ResultsReporter(Capabilities capabilities, RemoteWebDriver driver, String testName){
		this.caps = capabilities;	
		this.driver = driver;
		this.testName = testName;
	}
	
	public synchronized static String reporterLog(String msg, boolean error) {
		if (error) logError(msg);
		else logThread(msg);
		Reporter.log(msg);
		return msg;
	}
	
	public static void logThread(String msg) {
		System.out.println(Thread.currentThread().getName() + ": " + msg);
	}
	
	public static void logError(String msg) {
		System.err.println(Thread.currentThread().getName() + ": " + msg);
	}

	/**
	 * Reports a failure of a test, to the HTML report and Excel DB.
	 * A screenshot of the failure is saved and embedded in the HTML report.
	 * A link to the screenshot is saved in the Excel resultSheet.
	 * @param expectedResult The {@link String} value of the expected result, which would have passed the test
	 * @param actualResult The {@link String} value of the actual result, which is not equal to the expected result
	 * @param params A {@link String} array (String[]), with the test name and parameters.
	 * @return A {@link String} path to the stored screenshot.
	 */
	public String reportFail(String expectedResult, String actualResult){
    	String message = "Value is: " + actualResult + ", Should be: " + expectedResult + ".";
		reporterLog("FAILED:" + message, true);
    	takeScreenshot();
    	String link = getWindTunnelReportLink();
		try {
			addRowToDetailedSheet(false, link, message);
		} catch (Exception e) {
			logError(e.getMessage());
		}
		finally {
			Assert.fail(message);
		}
		return link;
	}
	
	/**
	 * Reports a failure of a test, to the HTML report and Excel DB.
	 * A screenshot of the failure is saved and embedded in the HTML report.
	 * A link to the screenshot is saved in the Excel resultSheet.
	 * @param messgage A {@link String} message with description of the failure.
	 * @param params A {@link String} array (String[]), with the test name and parameters.
	 */
	public void reportFailWithMessage(String message){
		reporterLog("FAILED: " + message + ".", true);
    	takeScreenshot();
    	String link = getWindTunnelReportLink();
		try {
			addRowToDetailedSheet(false, link, message);
		} catch (Exception e) {
			logError("Error updating spreadsheet:" + e.getMessage());
		}
		finally {
			Assert.fail(message);
		}
	}
	
	/**
	 * Reports a success of a test, to the HTML report and Excel DB.
	 * A screenshot of the checkpoint is saved and embedded in the HTML report.
	 * A link to the screenshot is saved in the Excel resultSheet.
	 * @param messgage A {@link String} message with description of the test.
	 * @param params A {@link String} array (String[]), with the test name and parameters.
	 */
	public void reportPass(String message){
		reporterLog("PASSED: " + message + ".", false);
    	String link = getWindTunnelReportLink();
		try {
			addRowToDetailedSheet(true, link, "");
		} catch (Exception e) {
			logThread(e.getMessage());
		}
	}
	/**
	 * Reports a transaction of a test, to the HTML report.
	 * A screenshot of the transaction is saved and embedded in the HTML report.
	 * @param messgage A {@link String} message with description of the transaction.
	 * @param params A {@link String} array (String[]), with the test name and parameters.
	 */
	public void reportMessage(String message){
		reporterLog(message + ".", false);
    	takeScreenshot();
	}

	public void takeScreenshot(){
		try {
			if (driver != null) {
				String screenshotinfo = PerfectoUtils.getScreenshotData(driver);
				Reporter.log("<br> <img src=\"data:image/png;base64," + screenshotinfo + "\" style=\"max-width:50%;max-height:50%\" /> <br>");
			}
			else {
				reporterLog("No driver open for screenshot", true);
			}
		} catch (Exception e1) {
			reporterLog("Error saving screenshot:\n " + e1.getMessage(), true);
		}
	}
	
	public String getWindTunnelReportLink(){
		try {
			String videoURLLink = caps.getCapability("windTunnelReportUrl") + "";
			if (videoURLLink.length() > 10) {
				logThread("WindTunnel Link:\n\t" + videoURLLink);
				Reporter.log("<a href=\"" + videoURLLink +"\">" + getDeviceDesc() + " Video</a>");
				return videoURLLink;
			}
			else {
				if (!PerfectoUtils.isDevice(caps)) return "Only available for MobileCloud devices";
				return reporterLog("WindTunnel Link N/A", true);
			}

		} catch (Exception e) {
			return reporterLog("Error finding WindTunnel link:\n " + e.getMessage().split("\n")[0], true);
		}

	}
	
	/**
	 * Adds a the test result to the Excel db sheet.
	 * Row includes:
	 * <ul>
	 * 	<li> Date and time
	 *  <li> Server URL
	 *  <li> User
	 *  <li> Resource type (Desktop/mobile)
	 *  <li> Manufacturer (Mobile only)
	 *  <li> model (Mobile only)
	 *  <li> OS type
	 *  <li> OS version (Mobile only)
	 *  <li> Carrier (Only for mobile)
	 *  <li> Browser version (Desktop only) 
	 *  <li> Test name
	 *  <li> Test params
	 *  <li> Test cycle
	 *  <li> Test result
	 * </ul>
	 * <p>
	 *  
	 */
	public static void addColumnsToDetailedSheet(){
		LinkedHashMap<String, String> testProperties = new LinkedHashMap<String, String>();
		testProperties.put("testName", "");
		testProperties.put("deviceName", "");
		testProperties.put("model", "");
		testProperties.put("testResult", "");
		testProperties.put("message", "");
		testProperties.put("time", "");
				
		try {
			detailedResultSheet = new ExcelDriver(Init.getProperty("detailedResultWorkbook"), "fullDetails", true);
			detailedResultSheet.addColumnsFromMap(testProperties);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void addRowToDetailedSheet(boolean isPassed, String windtunnel, String message) throws Exception{
		String time = PerfectoUtils.getDateAndTimeByFormat("yyyy-MM-dd HH:mm:ss", 0);
		String testResult = isPassed? "PASS" : "FAIL";
		LinkedHashMap<String, String> testProperties = new LinkedHashMap<String, String>();
		testProperties.put("deviceName", caps.getCapability("deviceName") + "");
		testProperties.put("model", caps.getCapability("model") + "");
		testProperties.put("phoneNumber", caps.getCapability("number") + "");
		testProperties.put("testName", this.testName);
		testProperties.put("time", time);
		testProperties.put("testResult", testResult);
		testProperties.put("message", (message + "").split("\n")[0]);
		testProperties.put("windTunnelReportUrl", windtunnel);
		detailedResultSheet.addResultsToDetailedSheet(testProperties);
		//detailedResultSheet.setAutoSize();
	}
}

package com.perfectomobile.test;

import com.perfectomobile.utils.PerfectoUtils;
import org.openqa.selenium.Capabilities;
import org.testng.Reporter;

import java.util.Arrays;
import java.util.Iterator;

public class ConsoleReporter {
	/**
	 * The device description as it's stored on the Perfecto Mobile cloud
	 */
	public static String getDeviceDesc(Capabilities caps) {
		if (caps.getCapability("deviceDbName") != null)
			return caps.getCapability("deviceDbName") + "";
		if (PerfectoUtils.isDevice(caps))
			return caps.getCapability("manufacturer") + ("" + caps.getCapability("model")).replace(" ", "") + " " + getDeviceName(caps);
		else return getPlatformName(caps) + " " + getDeviceName(caps) + " " + getVersion(caps);
	}
	
	public static String getDeviceName(Capabilities caps) {
		return caps.getCapability("deviceName") == null ? 
				(caps.getCapability("deviceDbName") == null ?
						(caps.getCapability("description") == null ? "" : caps.getCapability("description") + "")
						: caps.getCapability("deviceDbName") + "")
				: caps.getCapability("deviceName") + "";
	}
	
	public static String getPlatformName(Capabilities caps) {
		return caps.getCapability("platformName") + "";/* == null ?
				(PerfectoUtils.isDesktopBrowser(caps) ?
						("ANY".equals(caps.getCapability("platform")) 
							? "Desktop" : caps.getCapability("platform") + "") 
						: (caps.getCapability("os") == null ? "Device" : caps.getCapability("os") +"")) 
				: caps.getCapability("platformName") + "";*/
	}
	
	public static String getVersion(Capabilities caps) {
		return caps.getCapability("platformVersion") == null ? 
				(caps.getVersion() == null || caps.getVersion().isEmpty() ?
						caps.getBrowserName() : caps.getVersion())
				: caps.getCapability("platformVersion") + "";
	}
	
	/**
	 * The name of the test function
	 */
	public static String getTestName(Capabilities caps) {
		return caps.getCapability("scriptName") == null ? "" : caps.getCapability("scriptName") + "";
	}

	public static String getThreadName(Capabilities caps){
		return String.format("%s[%s]", getTestName(caps).split(" ")[0], String.format("%5.15s", getDeviceDesc(caps).replace("null", "")));
	}
	
	public static void setThreadName(Capabilities caps) {
		Thread.currentThread().setName(getThreadName(caps)); 
	}
	
	public static synchronized void logThread(String msg) {
		Reporter.log(msg);
		System.out.println(Thread.currentThread().getName() + ": " + msg);
	}
	
	public static synchronized void logError(String msg) {
		logError(msg, 1);
	}
	
	public static synchronized void logFullError(String msg) {
		logError(msg, 10000);
	}

	public static void logError(String msg, int numErrorLinesToPrint) {
		Iterator<String> msgLines = Arrays.asList((msg + "").split("\n")).listIterator();
		msg = "";
		while (numErrorLinesToPrint-- > 0 && msgLines.hasNext()) {
			msg += msgLines.next();
			Reporter.log(msg, false);
			System.err.println(Thread.currentThread().getName() + ": " + msg);
			msg = "\t";
		}
	}

}

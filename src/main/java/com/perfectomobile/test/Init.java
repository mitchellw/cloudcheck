package com.perfectomobile.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class Init {

	private static Properties prop;
	private static HashMap<String,String> sysProp;
	
	private Init(){
		prop();
	}
	
	public static HashMap<String, String> prop() {
		prop = new Properties();
		sysProp = new HashMap<String, String>();
		
		try {
			prop.load(new FileInputStream("src/main/resources/framework.properties"));
			 for (String key : prop.stringPropertyNames()) {
			      String value = prop.getProperty(key);
			      sysProp.put(key, value);
			 }
			 /* Set Hash Map */
			setSysProp(sysProp);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		/* The sysProp is only used for the dataprovided initially as TestNG runs the data provided before the constructor */
		return sysProp;
	}
	
	private static class SingletonHolder { 
		private static final Init INSTANCE = new Init();
	}

	public static Init getInstance() {
	    return SingletonHolder.INSTANCE;
	}
	
	public HashMap<String, String> getSysProp() {
		return sysProp;
	}
	
	public void setSysProp(String key, String value) {
		sysProp.put(key, value);
	}
	
	public static String getProperty(String prop) {
		return getInstance().getSysProp().get(prop);
	}
	
	public static void setSysProp(HashMap<String, String> sysProp) {
		Init.sysProp = sysProp;
	}

}

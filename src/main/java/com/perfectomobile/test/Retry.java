package com.perfectomobile.test;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;
import org.testng.Reporter;

public class Retry implements IRetryAnalyzer {
    private int retryCount = 0;

    public boolean retry(ITestResult result) {
    	try {
			Object retries = Init.getProperty("testFailureRetries");
			if (retries == null) return false;
			int maxRetryCount = Integer.parseInt(Init.getProperty("testFailureRetries"));
			if (++retryCount <= maxRetryCount) {
			    Reporter.log("Thread-" + Thread.currentThread().getId() +": Retry #" + retryCount + " for test: " + result.getMethod().getMethodName(), true);
			    return true;
			}
			return false;
		} catch (NumberFormatException e) {
			return false;
		}
    }
}
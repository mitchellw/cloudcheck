package com.perfectomobile;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.perfectomobile.driver.AppiumDriverExtended;
import org.apache.commons.collections.bag.SynchronizedBag;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.varia.NullAppender;
import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.IAnnotationTransformer;
import org.testng.ITest;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.ITestAnnotation;
import org.testng.internal.ClassHelper;
import org.testng.internal.TestNGMethod;
import org.testng.internal.annotations.AnnotationHelper;
import org.testng.internal.annotations.IAnnotationFinder;
import org.testng.internal.annotations.JDK15AnnotationFinder;

import com.perfectomobile.httpclient.Credentials;
import com.perfectomobile.httpclient.HttpClientException;
import com.perfectomobile.httpclient.ParameterValue;
import com.perfectomobile.httpclient.device.DeviceParameter;
import com.perfectomobile.httpclient.device.DeviceResult;
import com.perfectomobile.httpclient.device.DevicesHttpClient;
import com.perfectomobile.test.Init;
import com.perfectomobile.test.RemoteWebDriverExtended;
import com.perfectomobile.test.ResultsReporter;
import com.perfectomobile.utils.PerfectoUtils;


public class DeviceFactory implements ITest {
	
	static HashMap<String,String> systemProps;
	static String host;
	static String username;
	static String password;
	private String testName;
	private String threadName;
	protected DeviceResult device;
	protected ResultsReporter testResults;
	protected AppiumDriverExtended driver;
	protected DesiredCapabilities dcaps;
	
	public DeviceFactory(DeviceResult device) {
		this.device = device;
	}

	@Override
	public String getTestName() {
		return threadName;
	}
	
	private static String getFullDeviceDesc(DeviceResult device) {
		return device.getResponseValue(DeviceParameter.MANUFACTURER).replace(" ", "")
				+ device.getResponseValue(DeviceParameter.MODEL).replace(" ", "") 
				+ "_" + device.getResponseValue(DeviceParameter.DEVICE_ID);
	}
	
	private String getdeviceDesc() {
		return String.format("%25.25s", getFullDeviceDesc(device));
	}
	
	@DataProvider (name = "deviceFactory", parallel = true)
	public static Object[][] searchItemsData(ITestContext testContext) throws Exception{
		setAllParameters(testContext);
		List<DeviceResult> deviceList = getDeviceList();
		Class<?> testClass = Class.forName(testContext.getCurrentXmlTest().getClasses().get(0).getName());
		List<String> selectedTestList = CheckSelectedTestMethods(testClass);
		if (selectedTestList.size() == 0) 
			ResultsReporter.logError("\n\nERROR: NO TESTS WERE RUN BECAUSE:\n\t"
					+ "NO Test Methods contained testSelect parameter: [" + System.getProperty("testSelect") 
					+ "]\n\tIn Test Class: [" + testClass.getName() + "]\n\t");
		else System.out.println();
		
		Object[][] data = new Object[deviceList.size()][1];
		int i = 0;
		for (DeviceResult device : deviceList) {
			data[i++][0] = device;
		}
		return data;
	}
	
	@BeforeMethod(alwaysRun=true)
	public void beforeMethod(Method method) {
		testName = method.getName();
		threadName = String.format("%s(%s)", method.getName(), getdeviceDesc());
		Thread.currentThread().setName(threadName); 
		setInitialCaps();
		dcaps.setCapability("scriptName", "CloudChecks_" + testName);
		testResults = new ResultsReporter(dcaps, testName);
		ResultsReporter.logThread("Test Started... ");
	}
	
	@AfterMethod(alwaysRun=true)
	public void afterMethod(Method method) {
		if (driver != null) {
			driver.close();
			driver.quit();
			driver = null;
			ResultsReporter.logThread("Device Closed/Quit!");
		}
	}
	
	@AfterTest(alwaysRun=true)
	public static void afterTest() throws Exception{
		ResultsReporter.getDetailedResultSheet().setAutoSize();
	}
	
	private DesiredCapabilities setInitialCaps(){
		dcaps = new DesiredCapabilities("", "", Platform.ANY);
		dcaps.setCapability("deviceName", device.getResponseValue(DeviceParameter.DEVICE_ID));
		dcaps.setCapability("os", device.getResponseValue(DeviceParameter.OS));
		dcaps.setCapability("model", device.getResponseValue(DeviceParameter.MODEL));
		dcaps.setCapability("number", device.getResponseValue(DeviceParameter.PHONE_NUMBER));
		return dcaps;
	}
	
	public static void setAllParameters(ITestContext testContext) {
		org.apache.log4j.BasicConfigurator.configure(new NullAppender());
		systemProps = Init.getInstance().getSysProp();
		Map<String, String> testParams = testContext.getCurrentXmlTest().getAllParameters();
		systemProps.putAll(new HashMap<>(testParams));

		for (String param : testParams.keySet()) {
			String envVar = System.getProperty(param);
			if (envVar != null && envVar.length() > 3)
				Init.getInstance().setSysProp(param, envVar);
		}
		host = systemProps.get("perfectoURL").replace("https://", "").replace("/nexperience/perfectomobile/wd/hub", "");
		username = systemProps.get("perfectoUserName");
		password = systemProps.get("perfectoPassword");
		
		ResultsReporter.logThread("\nStarting Tests on Cloud: " + host + "...\n");
		ResultsReporter.logThread("System properties set:\n\ttestSelect=" + System.getProperty("testSelect"));
		systemProps.forEach((k, v) 
				-> System.out.println("\t" + k + "=" + (k.equals("perfectoPassword") 
						? StringUtils.repeat('*', password.length()) : v)));
		ResultsReporter.addColumnsToDetailedSheet();
	}
	
	public static List<DeviceResult> getDeviceList() throws Exception {
		
		List<ParameterValue> inputParameters = new LinkedList<>();
		if (!"true".equalsIgnoreCase(systemProps.get("forceDevice")))
			inputParameters.add(new ParameterValue("availableTo", username));
		String delimitedCaps = System.getProperty("deviceCaps") + "";
		for (String addCap : delimitedCaps.split(",")){
			String[] splitKeyValue = addCap.split("=");
			if (splitKeyValue.length > 1) {
				ParameterValue pval = new ParameterValue(splitKeyValue[0], splitKeyValue[1]);
				inputParameters.add(pval);
			}
		}
		//inputParameters.add(new ParameterValue("description", "Mitchell"));
		ResultsReporter.logThread("Device Selection Parameters:\t");
		inputParameters.forEach(x -> System.out.println("\t" + x.toString()));
		List<DeviceResult> deviceList = null;
		
		String exceptionMsg = "\n\nERROR: NO TESTS RAN BECAUSE:\n\t";
		try {
			Credentials credentials = new Credentials(username, password);
			DevicesHttpClient client = new DevicesHttpClient(host, credentials);
			deviceList = client.listDevices(inputParameters, false);
		} catch (Exception e) {
			exceptionMsg += (e.getMessage() + "").split("\n")[0] 
					+ "\n\tCheck Username=" + username + ", Password=" 
					+ StringUtils.repeat('*', password.length()) + ", For host: " + host;
			ResultsReporter.reporterLog(exceptionMsg + "\n\n", true);
			throw e;
		}
		
		if (deviceList == null || deviceList.size() == 0) {
			exceptionMsg += "No device(s) found in cloud that match parameters:\n\t"
					+ StringUtils.join(inputParameters, "\n\t");
			ResultsReporter.reporterLog(exceptionMsg + "\n\n", true);
			throw new Exception(exceptionMsg);
		}
		else {
			ResultsReporter.reporterLog("Found [" + deviceList.size() + "] Devices for testing:", false);
			deviceList.forEach(x -> System.out.println("\t" + getFullDeviceDesc(x)));
		}
		
		return deviceList;
	}
	
	public void testDevice(String command, HashMap<Object,Object> params) {
		startDeviceDriver();
		try {
			if (command == null || command.isEmpty() || dcaps.getBrowserName().equals("MobileOS")) {
				ResultsReporter.logThread("Opening browser to community");
				driver.get("community.perfectomobile.com");
			}
			else {
				ResultsReporter.logThread("Executing command: " + command + ", with parameters: " + params.toString());
				driver.executeScript(command, params);
				ResultsReporter.logThread("Command: " + command + " completed.");
			}
		}
		catch (Exception e){
			testResults.reportFailWithMessage((e.getMessage() + "").split("\n")[0]);
		}
		String message = "Test Finished";
		testResults.reportPass(message);
	}
	
	protected AppiumDriverExtended startDeviceDriver() {
		try {
			testResults.setCaps(dcaps);
			driver = PerfectoUtils.getDriver(dcaps, Integer.parseInt(systemProps.get("driverRetries")), Integer.parseInt(systemProps.get("retryIntervalSeconds")));
			testResults.setDriver(driver);
			testResults.setCaps(driver.getCapabilities());
			return driver;
		}
		catch (Exception e){
			e.printStackTrace();
			testResults.reportFailWithMessage((e.getMessage() + "").split("\n")[0]);
			return null;
		}
	}

    public static List<String> CheckSelectedTestMethods(Class<?> testClass) {
        IAnnotationFinder finder = new JDK15AnnotationFinder(new DummyTransformer());
        Set<Method> allMethods = ClassHelper.getAvailableMethods(testClass);
        List<String> testMethodNames = new ArrayList<String>();
        String testSelect = System.getProperty("testSelect");
        ResultsReporter.logThread("The following tests will be run:");
        for (Method testngMethod : allMethods) {
            ITestAnnotation value = AnnotationHelper.findTest(finder, testngMethod);
            if (value != null) {
            	boolean selected = (System.getProperty("testSelect") == null 
               			|| System.getProperty("testSelect").length() < 3
               			|| System.getProperty("testSelect").equals("All")) 
               			? (!testngMethod.getName().contains("Reboot")
               			&& !testngMethod.getName().contains("SwitchWifi")
               			&& !testngMethod.getName().contains("InstallApp"))
               			: testngMethod.getName().contains(System.getProperty("testSelect"));
                if (selected) {
                	testMethodNames.add(testngMethod.getName());
                	System.out.println("\t" + testngMethod.getName()); 
                }
            }
        }
        return testMethodNames;
    }

    public static class DummyTransformer implements IAnnotationTransformer {

        @SuppressWarnings("rawtypes")
		@Override
		public void transform(ITestAnnotation annotation, Class testClass, Constructor testConstructor,
				Method testMethod) {
		}
    }
}



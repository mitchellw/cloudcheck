package com.perfectomobile;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.deser.Deserializers;
import com.perfectomobile.driver.AppiumDriverExtended;
import com.perfectomobile.test.ConsoleReporter;
import com.perfectomobile.utils.PerfectoLabUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

import com.perfectomobile.httpclient.device.DeviceParameter;
import com.perfectomobile.httpclient.device.DeviceResult;
import com.perfectomobile.test.RemoteWebDriverExtended;
import com.perfectomobile.test.ResultsReporter;
import com.perfectomobile.utils.PerfectoUtils;
import com.perfectomobile.utils.PopUpUtils;
import com.perfectomobile.utils.PopUpUtils.Context;
import tng.pages.BasePage;
import tng.pages.LoginPage;

import static com.perfectomobile.utils.PerfectoLabUtils.*;

public class CloudChecks extends DeviceFactory {

	@Factory(dataProvider = "deviceFactory")
	public CloudChecks(DeviceResult device) {
		super(device);
		ResultsReporter.logThread("DEVICE DETAILS:\n\t" + device.toString());
	}

	@Test(priority=0)
	public void Reboot() {
		testDevice("mobile:handset:reboot", new HashMap<>());
	}
	
	@Test(priority=1)
	public void Swipe() {
		try {
			startDeviceDriver();

			driver.executeScript("mobile:handset:ready", new HashMap<>());

			Map<String, Object> textParams = new HashMap<>();
			textParams.put("context", "all");
			Object home = driver.executeScript("mobile:screen:text", textParams);

			HashMap<Object, Object> swipeParams = new HashMap<>();
			swipeParams.put("start", "50%,1%");
			swipeParams.put("end", "50%,90%");
			driver.executeScript("mobile:touch:swipe", swipeParams);
			
			Thread.sleep(4000);

			Object rightScreen = driver.executeScript("mobile:screen:text", textParams);
			
			if (!home.equals(rightScreen))
				testResults.reportPass("Swipe down successful");
			else
				testResults.reportFailWithMessage("Text on screens were the same so swipe failed.");
		} catch (Exception e) {
			// e.printStackTrace();
			testResults.reportFailWithMessage(e.getMessage());
		}

	}

	@Test(invocationCount = 10)
	public void runPOM() {
		setExecutionIdCapability(dcaps,"tangerinetest.perfectomobile.com", Invoker.INTELLIJ);
		StopWatch statusTimer = BasePage.startTimer();
		new LoginPage(startDeviceDriver())
				.GO("18355191")
				.typeSecretAnswer()
				.typePIN("123123")
				.acceptTOS()
				.logOff();
		ConsoleReporter.logThread("TEST COMPLETED IN: " + BasePage.getTimerSeconds(statusTimer) + " SECONDS!");
	}

	@Test(priority=2)
	public void InstallApp() {
		String[] details = System.getProperty("appDetails").split(",");
		dcaps.setCapability("app", details[1]);
		if (device.getResponseValue(DeviceParameter.OS.toString().toLowerCase()).equalsIgnoreCase("android"))
			dcaps.setCapability("appPackage", details[2]);
		else if (device.getResponseValue(DeviceParameter.OS.toString().toLowerCase()).equalsIgnoreCase("ios"))
			dcaps.setCapability("bundleId", details[2]);
		
		dcaps.setCapability("autoInstrument", true);

		HashMap<Object, Object> params = new HashMap<>();
		params.put("name", details[0]);
		testDevice("mobile:application:close", params);
	}
	
	@Test(priority=2)
	public void WindTunnelLocation() {
		dcaps.setCapability("windTunnelPersona", "Empty");
		dcaps.setCapability("windTunnelLocationAddress", "Dallas, TX");

		testDevice("mobile:location:reset", new HashMap<>());
	}

	@Test(priority=2)
	public void WindTunnelNetworkCondition() {
		dcaps.setCapability("browserName", "MobileOS");
		dcaps.setCapability("windTunnelPersona", "Empty");
		dcaps.setCapability("windTunnelVNetwork", "4g_lte_average");

		testDevice("", null);
	}
	
	@Test(priority=2)
	public void UseWindTunnelPersona() {
		String name = System.getProperty("personaName");
		if (name == null || name.isEmpty()) {
			ResultsReporter.logThread("No persona name specified!!!  Using EMPTY.");
			name = "Empty";
		}
		else ResultsReporter.logThread("Specified WindTunnel Persona: " + name);
		dcaps.setCapability("browserName", "MobileOS");
		dcaps.setCapability("windTunnelPersona", name);

		testDevice("", null);
	}

	@Test(priority=1)
	public void ResetNetwork() {
		dcaps.setCapability("browserName", "MobileOS");
		
		HashMap<Object, Object> params4 = new HashMap<>();
		params4.put("wifi", "enabled");
		params4.put("data", "enabled");
		params4.put("airplanemode", "disabled");

		testDevice("mobile:network.settings:set", params4);
	}
	
	@Test(priority=3)
	public void SwitchWifi() {
		try{
			String wifiName = "pm1243-5GHz";
			if (System.getProperty("wifiName") != null)
				wifiName = System.getProperty("wifiName");
			
			startDeviceDriver();
			
			PopUpUtils networkPopup = new PopUpUtils(driver);
			networkPopup.switchToContext(Context.NATIVE_APP);
			
			PerfectoUtils.startApp("Settings", driver);
			
			networkPopup.setNativePopupBtns(By.xpath("//*[@text='" + wifiName + "']"),
					By.xpath("//(*[contains(@text,'Wi') and contains(@text,'Fi')])"),
					By.name("Wi-Fi"));

			if (!networkPopup.waitForPopupAndClick(10))
				throw new Exception("Wi-Fi Btn NOT Found!");

			String browserName = "Chrome";
			
			int retries = 0;
			
			if (dcaps.getCapability("os").toString().toLowerCase().equals("android")) {
				browserName = "Chrome";
				networkPopup.setNativePopupBtns(By.xpath("//android.widget.Button[@text='Connect']"),
						By.xpath("//android.widget.Button[@text='Disconnect']"),
						By.xpath("//android.widget.Button[@text='Forget']"));
				
				By fiveGHzLocator = By.xpath("//android.widget.TextView[@text='" + wifiName + "']");
				
				while (true) {
					if (retries++ > 5)
						throw new Exception("Unable to join " + wifiName + "Network!!");
					ResultsReporter.logThread("Looking for " + wifiName + " network...");
					WebElement network = PerfectoUtils.fluentWait(fiveGHzLocator, driver, 5);
					if (network != null) {
						network.click();
						networkPopup.waitForPopupAndClick(5);
					}
					else continue;
					ResultsReporter.logThread("Waiting for " + wifiName + " Connection Status...");
					if (PerfectoUtils.fluentWait(
							By.xpath("//android.widget.TextView[@text='" + wifiName + "']/following-sibling::*[@text='Connected']")
							, driver, 15) != null) 
					{
						ResultsReporter.logThread("Connected to " + wifiName + " Network!");
						break;
					}
				}
				PerfectoUtils.changeNetworkSettings(true, false, false, driver);
			}
			
			else if (dcaps.getCapability("os").toString().toLowerCase().equals("ios")) {
				browserName = "Safari";
				while (true) {
					if (retries++ > 5)
						throw new Exception("Unable to join " + wifiName + " Network!!");
					ResultsReporter.logThread("Looking for " + wifiName + " network...");
					WebElement network = PerfectoUtils.fluentWait(By.name(wifiName), driver, 5);
					if (network != null) {
						ResultsReporter.logThread("Selecting " + wifiName + " Network...");
						network.click();
						if (new PopUpUtils(driver).setNativePopupBtns(By.name("Dismiss"), By.name("Join Network")).waitForPopupAndClick(5)) {
							ResultsReporter.logThread("Connection attempt failed.  Retrying...");
							continue;
						}
						else if (new PopUpUtils(driver).setNativePopupBtns(By.name("Forget This Network")).waitForPopupAndClick(2)) {
							ResultsReporter.logThread("Removing existing network connection...");
							new PopUpUtils(driver).setNativePopupBtns(By.name("Forget"), By.name("Wi-Fi")).clickMulitplePopUps(2, 2);
							continue;
						}
					}
					else continue;
					ResultsReporter.logThread("Waiting for " + wifiName + " Connection Status...");
					if (PerfectoUtils.fluentWait(
							By.xpath("(//UIAStaticText[@name='Wi-Fi'])[2]/following::UIAStaticText[1][@name='" + wifiName + "']")
							, driver, 5) != null) 
					{
						ResultsReporter.logThread("Connected to " + wifiName + " Network!");
						break;
					}
				}
			}
			
			PerfectoUtils.closeApp("Settings", driver);
			
			ResultsReporter.logThread("Starting " + browserName);
			PerfectoUtils.startApp(browserName, driver);
			
			//if (!browserName.equals("Safari")) {
			//	ResultsReporter.logThread("Cleaning " + browserName);
			//	PerfectoUtils.cleanBrowser(driver);
			//}
			
			/*ResultsReporter.logThread("Closing " + browserName);
			PerfectoUtils.closeApp(browserName, driver);*/
			
			//if(checkIfBrowserIsOnline())
			//	testResults.reportPass("Browser Online");
			//else testResults.reportFailWithMessage("Browser time was not found or off indicating no connection");
		}
		catch (Exception e){
			//e.printStackTrace();
			testResults.reportFailWithMessage((e.getMessage() + "").split("\n")[0]);
		}
	}
	
	public boolean checkIfBrowserIsOnline() {
		
		//ResultsReporter.logThread("Cleaning browser...");
		if (device.getResponseValue(DeviceParameter.OS).equalsIgnoreCase("ios")) 
			PerfectoUtils.startApp("Safari", driver);
		else if (device.getResponseValue(DeviceParameter.OS).equalsIgnoreCase("android")) 
			PerfectoUtils.startApp("Chrome", driver);

		
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		ResultsReporter.logThread("Navigating to Community...");
		driver.get("community.perfectomobile.com");
		
		//new PopUpUtils(driver).clearNewChromePages();
		
		driver.setContext(AppiumDriverExtended.ContextType.WEBVIEW);
		new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[@class='icon ic-search']/parent::button)[1]")));
		
		ResultsReporter.logThread("Navigating to www.clocktab.com to compare time...");
		driver.get("www.clocktab.com");
		
		WebElement time = new WebDriverWait(driver,20).until(ExpectedConditions.visibilityOfElementLocated(By.id("time")));
		
		int currentMin = Integer.parseInt(new SimpleDateFormat("HH:mm").format(new Date()).split(":")[1]);
		
		int browserMin = Integer.parseInt(time.getText().split(":")[1]);
		
		String message = "Minute comparison: " + currentMin + " vs " + browserMin;
		ResultsReporter.logThread(message);
		
		return Math.abs(currentMin - browserMin) < 3 || Math.abs(currentMin - browserMin) > 57;
	}
	
	@Test(priority=1)
	public void isBrowserOnline() {
		try{
			dcaps.setCapability("browserName", "MobileOS");
			startDeviceDriver();
			PerfectoUtils.changeNetworkSettings(true, true, false, driver);
			if(checkIfBrowserIsOnline()) testResults.reportPass("Browser Online");
			else testResults.reportFailWithMessage("Browser time was not found or off indicating no connection");
		} catch (Exception e) {
			String error = (e.getMessage() + "").split("\n")[0];
			if (error.contains("no visible webview")) 
				if (device.getResponseValue(DeviceParameter.OS).toLowerCase().equals("ios"))
					error += "\n\tEnsure Web Inspector is enabled for iOS devices.";
			// e.printStackTrace();
			testResults.reportFailWithMessage(error);
		}
	}

	@Test(priority=2)
	public void deviceRotate() {
		try {
			dcaps.setCapability("browserName", "MobileOS");
			dcaps.setCapability("orientation", "LANDSCAPE");
			startDeviceDriver();

			Map<String, Object> textParams = new HashMap<>();
			textParams.put("context", "all");
			Object home = driver.executeScript("mobile:screen:text", textParams);

			HashMap<Object, Object> paramsMap = new HashMap<>();
			paramsMap.put("operation", "reset");
			driver.executeScript("mobile:handset:rotate", paramsMap);
			
			Thread.sleep(2000);

			Object rightScreen = driver.executeScript("mobile:screen:text", textParams);
			
			if (!home.equals(rightScreen))
				testResults.reportPass("Rotate successful");
			else
				testResults.reportFailWithMessage("Text on screens were the same so Rotate failed.");
		} catch (Exception e) {
			// e.printStackTrace();
			testResults.reportFailWithMessage(e.getMessage());
		}
	}

	@Test(priority=4)
	public void CloudCall() {
		try{
			if (!validPhoneNumber()) return;

			HashMap<Object, Object> callParams = new HashMap<>();
			callParams.put("to.number", device.getResponseValue(DeviceParameter.PHONE_NUMBER));

			startDeviceDriver();

			driver.executeScript("mobile:gateway:call", callParams);

			Map<String, Object> ocrParams = new HashMap<>();
			ocrParams.put("content", "972771111111");
			ocrParams.put("timeout", "60");
			String found = driver.executeScript("mobile:text:find", ocrParams) + "";

			if (found.equals("true"))
				testResults.reportPass("Cloud Call received");
			else
				testResults.reportFailWithMessage("Cloud Call NOT recieved in 60 sec");
		} catch (Exception e) {
			testResults.reportFailWithMessage(e.getMessage());
		}
	}

	@Test(priority = 4)
	public void CloudTxt() {
		try {
			if (!validPhoneNumber())
				return;

			String txtMsg = "CloudCheck Test TXT";

			HashMap<Object, Object> txtParams = new HashMap<>();
			txtParams.put("body", txtMsg);
			txtParams.put("to.number", device.getResponseValue(DeviceParameter.PHONE_NUMBER));

			startDeviceDriver();

			driver.executeScript("mobile:gateway:sms", txtParams);

			Map<String, Object> ocrParams = new HashMap<>();
			ocrParams.put("content", txtMsg);
			ocrParams.put("timeout", "60");
			String found = driver.executeScript("mobile:text:find", ocrParams) + "";

			if (!found.equals("true"))
				testResults.reportPass("Txt message received");
			else
				testResults.reportFailWithMessage("Txt message NOT recieved in 60 sec");
		} catch (Exception e) {
			testResults.reportFailWithMessage(e.getMessage());}
	}
	
	private boolean validPhoneNumber() {
		String phoneNumber = device.getResponseValue(DeviceParameter.PHONE_NUMBER);
		if (phoneNumber == null || phoneNumber.isEmpty()) {
			testResults.reportPass("Device does NOT have phone number!");
			return false;
		}
		else if (!phoneNumber.startsWith("+")){
			testResults.reportFailWithMessage("Phone number does not start with '+': " + DeviceParameter.PHONE_NUMBER);
		}
		return true;
	}
}
